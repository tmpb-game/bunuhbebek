﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This script is in charge of playing music in the game
/// </summary>
public class MusicPlayer : MonoBehaviour
{
    /// <summary>
    /// The clip to play in a menu.
    /// This field is private because it's not designed to be directly
    /// modified by other scripts, and tagged with [SerializeField] so that
    /// you can still modify it using the Inspector and so that Unity
    /// saves its value.
    /// </summary>
    [SerializeField]
    private AudioClip menuMusic;

    /// <summary>
    /// The clip to play outside menus.
    /// </summary>
    [SerializeField]
    private AudioClip levelMusic;

    /// <summary>
    /// The clip to play outside menus.
    /// </summary>
    [SerializeField]
    private AudioClip gameoverMusic;

	/// <summary>
	/// The clip to play outside menus.
	/// </summary>
	[SerializeField]
	private AudioClip alertMusic;

	/// <summary>
	/// The clip to play outside menus.
	/// </summary>
	[SerializeField]
	private AudioClip bossMusic;

	/// <summary>
	/// The clip to play outside menus.
	/// </summary>
	[SerializeField]
	private AudioClip winMusic;

    [SerializeField]
    /// <summary>
    /// The component that plays the music
    /// </summary>
    private AudioSource source;

    /// <summary>
    /// This class follows the singleton pattern and this is its instance
    /// </summary>
    static private MusicPlayer instance;

    /// <summary>
    /// Awake is not public because other scripts have no reason to call it directly,
    /// only the Unity runtime does (and it can call protected and private methods).
    /// It is protected virtual so that possible subclasses may perform more specific
    /// tasks in their own Awake and still call this base method (It's like constructors
    /// in object-oriented languages but compatible with Unity's component-based stuff.
    /// </summary>
    protected virtual void Awake()
    {
        // Singleton enforcement
        if (instance == null)
        {
            // Register as singleton if first
            instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            // Self-destruct if another instance exists
            Destroy(this);
            return;
        }
    }

    protected virtual void Start()
    {
        // If the game starts in a menu scene, play the appropriate music
		PlayMenuMusic();
    }

    static public bool IsOnPlay()
    {
        return instance.source.isPlaying;
    }

    /// <summary>
    /// Plays the music designed for the menus
    /// This method is static so that it can be called from anywhere in the code.
    /// </summary>
    static public void PlayMenuMusic()
    {
        if (instance != null)
        {
            if (instance.source != null)
            {
                instance.source.Stop();
                instance.source.clip = instance.menuMusic;
                instance.source.volume = 1f;
                instance.source.Play();
            }
        }
        else
        {
            Debug.LogError("Unavailable MusicPlayer component");
        }
    }

    /// <summary>
    /// Plays the music designed for outside menus
    /// This method is static so that it can be called from anywhere in the code.
    /// </summary>
    static public void PlayGameMusic()
    {
        if (instance != null)
        {
            if (instance.source != null)
            {
                instance.source.Stop();
                instance.source.clip = instance.levelMusic;
                instance.source.volume = 1f;
                instance.source.Play();
            }
        }
        else
        {
            Debug.LogError("Unavailable MusicPlayer component");
        }
    }

    /// <summary>
    /// Plays the music designed for outside menus
    /// This method is static so that it can be called from anywhere in the code.
    /// </summary>
    static public void PlayGameOverMusic()
    {
        if (instance != null)
        {
            if (instance.source != null)
            {
                instance.source.Stop();
				instance.source.clip = instance.gameoverMusic;
                instance.source.volume = 1f;
                instance.source.Play();
            }
        }
        else
        {
            Debug.LogError("Unavailable MusicPlayer component");
        }
    }


	/// <summary>
	/// Plays the music designed for outside menus
	/// This method is static so that it can be called from anywhere in the code.
	/// </summary>
	static public void PlayBossMusic()
	{
		if (instance != null)
		{
			if (instance.source != null)
			{
				instance.source.Stop();
				instance.source.clip = instance.bossMusic;
                instance.source.volume = 0.3f;
				instance.source.Play();
			}
		}
		else
		{
			Debug.LogError("Unavailable MusicPlayer component");
		}
	}

	/// <summary>
	/// Plays the music designed for outside menus
	/// This method is static so that it can be called from anywhere in the code.
	/// </summary>
	static public void PlayAlertMusic()
	{
		if (instance != null)
		{
			if (instance.source != null)
			{
				instance.source.Stop();
				instance.source.clip = instance.alertMusic;
				instance.source.volume = 1f;
				instance.source.Play();
			}
		}
		else
		{
			Debug.LogError("Unavailable MusicPlayer component");
		}
	}


	/// <summary>
	/// Plays the music designed for outside menus
	/// This method is static so that it can be called from anywhere in the code.
	/// </summary>
	static public void PlayWinMusic()
	{
		if (instance != null)
		{
			if (instance.source != null)
			{
				instance.source.Stop();
				instance.source.clip = instance.winMusic;
				instance.source.volume = 1f;
				instance.source.Play();
			}
		}
		else
		{
			Debug.LogError("Unavailable MusicPlayer component");
		}
	}}
