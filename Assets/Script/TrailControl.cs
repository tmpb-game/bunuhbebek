﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailControl : MonoBehaviour {

	public GameObject GhostTrail;

	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Trailing(){
		GameObject ins = Instantiate (GhostTrail, new Vector2 (transform.position.x, transform.position.y), new Quaternion ());
		ins.GetComponent<SpriteRenderer>().flipX = gameObject.GetComponent<SpriteRenderer> ().flipX;
	}

    // Update is called once per frame
    void TrailingRage()
    {
        GameObject ins = Instantiate(GhostTrail, new Vector2(transform.position.x, transform.position.y), new Quaternion());
        ins.GetComponent<SpriteRenderer>().flipX = gameObject.GetComponent<SpriteRenderer>().flipX;
        ins.GetComponent<SpriteRenderer>().color = new Vector4(240, 0, 0, 0.2f); 
    }

    public void ActivateTrailing(bool isRage=false)
    {
        if (!isRage)
        { InvokeRepeating("Trailing", 0.05f, 0.05f); }
        else
        { InvokeRepeating("TrailingRage", 0.05f, 0.05f); }
    }

    public void DeactivateTrailing()
    {
        CancelInvoke();
    }
}
