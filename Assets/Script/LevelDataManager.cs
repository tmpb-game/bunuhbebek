﻿using System;
using System.Collections;
using System.IO;
using UnityEngine;

public class LevelDataManager : MonoBehaviour
{
    private string path;
    private string jsonString;
    private int activeLevel;
    private LevelData levelData;
    public static LevelDataManager instance;

    //Singleton
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        LoadData();
    }

    void LoadData()
    {
        path = Application.streamingAssetsPath + "/levelData.json";
        jsonString = File.ReadAllText(path);
        levelData = JsonUtility.FromJson<LevelData>(jsonString);
    }

    public LevelData GetLevelData()
    {
        return levelData;
    }

    public LevelObject GetActiveLevelObject()
    {
        return levelData.levels[0];
    }

    public void SetActiveLevel(int levelIdx)
    {
        activeLevel = levelIdx - 1;
    }

    public void UpdateLevelData(int score, int stars)
    {
        levelData.levels[activeLevel].achievedScore = score;
        levelData.levels[activeLevel].achievedStar = stars;
        if (activeLevel + 1 < levelData.levels.Length)
            levelData.levels[activeLevel + 1].locked = false;
        File.WriteAllText(Application.streamingAssetsPath + "/levelData.json",  JsonUtility.ToJson(levelData));
    }

    public int FindLastUnlockedLevel()
    {
        int levelIdx = 0;
        for (int i = 0; i < levelData.levels.Length; i++)
        {
            if (levelData.levels[i].locked)
            {
                levelIdx = levelData.levels[i - 1].level;
                break;
            }
        }
        return levelIdx;
    }

    public int GetAchievedStars(int levelIdx)
    {
        return levelData.levels[levelIdx - 1].achievedStar;
    }
}