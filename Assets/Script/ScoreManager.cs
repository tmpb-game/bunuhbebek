﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    public Sprite[] SpriteScore;
    public int Score;

    private GameObject[] scoreBoard;
    private const int MaximumScore = 99999;

    void Start()
    {
        scoreBoard = new GameObject[MaximumScore.ToString().Length];
        for (int i = 0; i < MaximumScore.ToString().Length; i++)
        {
            scoreBoard[i] = GameObject.Find("Score" + (i + 1).ToString());
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Score > MaximumScore)
        { Score = MaximumScore; }

        char[] _scoreC = Score.ToString().ToCharArray();
        Array.Reverse(_scoreC);
        for (int i = 0; i < MaximumScore.ToString().Length; i++)
        {
            if (i < _scoreC.Length)
            {
                scoreBoard[i].GetComponent<SpriteRenderer>().sprite = SpriteScore[int.Parse(_scoreC[i].ToString())];
            }
            else
            { scoreBoard[i].GetComponent<SpriteRenderer>().sprite = SpriteScore[0]; }
        }
    }

    public int CalculateFinalScore(float timeleft, int hp, int type=0)
    {
        return (int)(Score + timeleft * 2 + hp * 10);
    }
}
