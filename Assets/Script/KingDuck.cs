﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class KingDuck : BossDuck
{
    //Adjust This
    private float defaultSpeed = 0.05f;
    private float maxSpeed = 0.2f;
    private int maxHp = 300;

    protected int inverterX = 1;
    private float deltaToJump = 3;

    private SpriteRenderer kingSpriteRender;
    private bool furiousMode = false;

    // Use this for initialization
    protected override void Start()
    {
        Speed = defaultSpeed;
        HitPoints = maxHp;
        shootedAnimName = "shootedking";

        kingSpriteRender = gameObject.GetComponent<SpriteRenderer>();

        base.Start();
    }

	float shootedDelay=0;
    // Update is called once per frame
    protected override void Update()
    {
        base.Update();

        if (HitPoints < (float)(maxHp / 3.0f))
        { EnterFuriousMode(); }

        if (this.duckAnimation.GetCurrentAnimatorStateInfo(0).IsName(shootedAnimName))
        {
			shootedDelay += Time.deltaTime;

			if (shootedDelay > 0.8f) {
				duckAnimation.SetBool ("IsShooted", false);
				shootedDelay = 0;
			}
        }
    }

    // Duck fly
    bool prevJumpState = false;
    float counterJump;
    protected override void Act()
    {
        float ics = GetComponent<Rigidbody2D>().transform.position.x;
        bool isOnJump = GetComponent<Rigidbody2D>().velocity.y == 0 ? false : true;
        bool isOnShooted = duckAnimation.GetBool("IsShooted");
        Vector2 position;
        
        if (!isOnJump)
        {
            //Shake Camera
            if (prevJumpState)
            {
                EarthquakeEffect();
                prevJumpState = false;

                //Reduce HP
                NotifyPlayerHp();

                if (furiousMode)
                { NotifyPlayerHp(); }//Bonus Damage
            }

            if (counterJump < deltaToJump || ics > 0 + 0.1f || ics < 0 - 0.1f)
            {
                position = new Vector2(Speed * inverterX, 0 );

                // boundaries control
                if (ics <= boundLeft + boundOffset)
                {
                    // Left
                    inverterX = 1;
                    GetComponent<SpriteRenderer>().flipX = true;
                }
                else if (ics >= boundRight - boundOffset)
                {
                    //Right
                    inverterX = -1;
                    GetComponent<SpriteRenderer>().flipX = false;
                }

                if (!isOnShooted)//g ngaruh
                {
                    // translate
                    GetComponent<Rigidbody2D>().transform.Translate(position);
                }

                counterJump += Time.deltaTime;
            }
            else
            {
                counterJump = 0;

                position = GetComponent<Rigidbody2D>().velocity;

                if (!furiousMode)
                { position.y = 5; }
                else
                { position.y = 10; }

                GetComponent<Rigidbody2D>().velocity = position;
            }
        }
        else
        {
            prevJumpState = true;
        }       
    }
   
    private void EarthquakeEffect()
    {
        EZCameraShake.CameraShaker _maincamera = GameObject.FindGameObjectWithTag("MainCamera").
                                                        GetComponent<EZCameraShake.CameraShaker>();
        _maincamera.ShakeOnce(0.1f, 12, 0.1f,3);

        GetComponent<CropSpreading>().StartSpread();

        //SFX
        if (SmashingGround != null)
        { SmashingGround.Play(); }
    }

    //Aladin Rage Mode
    private void EnterFuriousMode()
    {
        //First Enter
        if (!furiousMode)
        { kingSpriteRender.color = new Vector4(240, 0, 0, 1); }

        //Set Rage Mode!!
        furiousMode = true;
    }
}

