﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Crop : MonoBehaviour
{
    public AudioSource Shooted;
    
    //private int HitPoints;

    //internal int SpawnIndex;

    private Animator cropAnimation;
    private string shootedAnimName = "shootedcrp";
  
    // Use this for initialization
    void Start()
    {
        cropAnimation = GetComponent<Animator>();

        // randomly change dir value
        InvokeRepeating("ChangeDirection", UnityEngine.Random.Range(1.0f, 5.0f), UnityEngine.Random.Range(3.0f, 8.0f));
    }

    // Update is called once per frame
    void Update()
    {
        if (this.cropAnimation.GetCurrentAnimatorStateInfo(0).IsName(shootedAnimName))
        {
            cropAnimation.SetBool("IsShooted", false);
        }
    }

    // Invert the dir value
    private void ChangeDirection()
    {
        GetComponent<SpriteRenderer>().flipX = !GetComponent<SpriteRenderer>().flipX;
    }

    void OnMouseDown()
    {
        cropAnimation.SetBool("IsShooted", false);//clear prev

        //SFX
        if (Shooted != null)
        { Shooted.Play(); }

        //Shoot Animation
        cropAnimation.SetBool("IsShooted", true);
    }
}

