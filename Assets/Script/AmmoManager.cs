﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class AmmoManager : MonoBehaviour
{
    public Sprite[] SpriteAmmo;
    public int Ammo;
	public bool IsOnReload;
	public GameObject AnimationReload;
    public GameObject[] AmmoBoard;
	public GameObject AmmoSymbol;

    private const int MaximumAmmo = 99;

    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
		if (IsOnReload) {
			AnimationReload.SetActive (true);
			for (int i = 0; i < AmmoBoard.Length; i++) {
				AmmoBoard[i].SetActive (false);
			}
			AmmoSymbol.SetActive (false);
		} else {
			AnimationReload.SetActive (false);
			for (int i = 0; i < AmmoBoard.Length; i++) {
				AmmoBoard[i].SetActive (true);
			}
			AmmoSymbol.SetActive (true);
		}

        if (Ammo > MaximumAmmo)
        { Ammo = MaximumAmmo; }

        char[] _scoreC = Ammo.ToString().ToCharArray();
        Array.Reverse(_scoreC);
        for (int i = 0; i < MaximumAmmo.ToString().Length; i++)
        {
            if (i < _scoreC.Length)
            {
                AmmoBoard[i].GetComponent<SpriteRenderer>().sprite = SpriteAmmo[int.Parse(_scoreC[i].ToString())];
            }
            else
            { AmmoBoard[i].GetComponent<SpriteRenderer>().sprite = SpriteAmmo[0]; }
        }
    }
}
