﻿using System;
using System.Collections;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class WinHandler : MonoBehaviour
{
    public AudioSource StarShown;
    private Text ScoreText;
    private Image Star1;
    private Image Star2;
    private Image Star3;

    int score = 0;
    float duration = 2f;
  
    private void Start()
    {
        ScoreText = GameObject.FindGameObjectWithTag("finalscoreboard").GetComponent<Text>();
        Star1 = GameObject.Find("StarLeft").GetComponent<Image>();
        Star2 = GameObject.Find("StarCenter").GetComponent<Image>();
        Star3 = GameObject.Find("StarRight").GetComponent<Image>();
        Star1.enabled = false;
        Star2.enabled = false;
        Star3.enabled = false;
    }

    private void Update()
    {
        ScoreText.text = score.ToString();
    }

    public void AnimateScoring(int target)
    {
        StartCoroutine("CountTo", target);
    }

    IEnumerator CountTo(int target)
    {
        int start = score;
        for (float timer = 0; timer < duration; timer += Time.deltaTime)
        {
            float progress = timer / duration;
            score = (int)Mathf.SmoothStep(start, target, progress);
            yield return null;
        }
        score = target;

        // Show Stars
        StartCoroutine("ShowStars");
    }

    IEnumerator ShowStars()
    {
        LevelObject level = LevelDataManager.instance.GetActiveLevelObject();
        int achievedStars = 0;
        if (score >= level.star1)
        {
            yield return new WaitForSeconds(0.5f);
            Star1.enabled = true;
            StarShown.Play();
            achievedStars++;
        }
        if (score >= level.star2)
        {
            yield return new WaitForSeconds(0.5f);
            Star2.enabled = true;
            StarShown.Play();
            achievedStars++;
        }
        if (score >= level.star3)
        {
            yield return new WaitForSeconds(0.5f);
            Star3.enabled = true;
            StarShown.Play();
            achievedStars++;
        }

        LevelDataManager.instance.UpdateLevelData(score, achievedStars);
    }
}
