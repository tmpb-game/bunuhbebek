﻿using System;

[System.Serializable]
public class LevelData
{
    public LevelObject[] levels;

    public LevelData()
    {

    }
}

[System.Serializable]
public class LevelObject
{
    public int level;
    public int star1;
    public int star2;
    public int star3;
    public int achievedScore;
    public int achievedStar;
    public Boolean locked;

    public LevelObject()
    {

    }
}

