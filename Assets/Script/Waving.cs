﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class Waving : MonoBehaviour
{
    public float OffsetTop;
    public float OffsetBottom;
    public float Step;
    public float Interval;

    private RectTransform target;
    private float initialYpos;

    private void Start()
    {
        target = GetComponent<RectTransform>();
        initialYpos = target.position.y;

        StartWavingLoop();
    }

    public void StartWavingLoop()
    {
        if (target != null)
        {
            StartCoroutine("WavingPosition");
        }
    }

    IEnumerator WavingPosition()
    {
        int togglemovement = 0;

        while (true)
        {
            for (int i = 0; i < 3;)
            {
                Vector3 v = target.position;
                if(v.y == initialYpos)
                {
                    i++;
                }

                if (v.y >= initialYpos + OffsetTop)
                    togglemovement = 0;
                else if (v.y <= initialYpos - OffsetBottom)
                    togglemovement = 1;

                if (togglemovement == 0)
                    v.y -= Step;
                else if (togglemovement == 1)
                    v.y += Step;

                target.position = v;
                yield return new WaitForSeconds(Interval);
            }
           // yield return new WaitForSeconds(0.1f);
        }

    }
}
