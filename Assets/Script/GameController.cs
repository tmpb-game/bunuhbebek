﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public GameObject gameOvertext;             
    public bool GameOver { get { return gameOver; } }
    [HideInInspector]
    public bool GameStarted = false;

    public int HealthPoint = 99;
    public int DistractionHealthPoint = 3;

    private bool gameOver = false;               //Is the game over?
    
    void Update()
    {
        if (HealthPoint <= 0 || DistractionHealthPoint <=0)
        {
            LevelGameOver();
        }
    }

    public void LevelGameOver()
    {
        if (!gameOver)
        {
            //Activate the game over text.
            gameOvertext.SetActive(true);
            //Set the game to be over.
            gameOver = true;

            MusicPlayer.PlayGameOverMusic();
        }
    }
}
