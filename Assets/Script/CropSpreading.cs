﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CropSpreading : MonoBehaviour
{
    public GameObject Victim;
    public int NumberOfVictim;

    private GameObject[] VictimThrow;
    private Vector2[] flyAwayLocation;
    protected float deltaToGround = 0.4f;

    // Use this for initialization
    void Start()
    {
        VictimThrow = new GameObject[NumberOfVictim];
        for (int i = 0; i < VictimThrow.Length; i++)
        {
            VictimThrow[i] = Instantiate(Victim, new Vector2(), new Quaternion());
        }
    }

    private void FixedUpdate()
    {
        if (isOnSpread)
        { Spread(); }
    }

    bool isOnSpread = false;
    public void StartSpread()
    {
        int inverter = 1;
        isOnSpread = false;
        counterSpread = 0;
        flyAwayLocation = new Vector2[VictimThrow.Length];

        //Randomize Lication
        for (int i = 0; i < VictimThrow.Length; i++)
        {
            if (inverter == 1) { inverter = -1; }
            else { inverter = 1; }

            VictimThrow[i].transform.position = new Vector2(UnityEngine.Random.Range(-1f, 1f), 0);
            flyAwayLocation[i] = new Vector2(UnityEngine.Random.Range(3f, 4f) * inverter, UnityEngine.Random.Range(3.2f, 4f));
        }

        isOnSpread = true;
    }

    private float counterSpread;
    private void Spread()
    {
        for (int i = 0; i < VictimThrow.Length; i++)
        {
            //Position
            float step = 0.1f * deltaToGround;
            Vector2 currentPos = VictimThrow[i].GetComponent<Rigidbody2D>().transform.position;

            //Rotation
            Vector3 currentRot = VictimThrow[i].GetComponent<Rigidbody2D>().transform.rotation.eulerAngles;
            Quaternion newRotation = new Quaternion();
            currentRot.z = currentRot.z + 30;
            newRotation.eulerAngles = currentRot;

            //Update
            VictimThrow[i].GetComponent<Rigidbody2D>().transform.position = Vector2.MoveTowards(currentPos, flyAwayLocation[i], step);
            VictimThrow[i].GetComponent<Rigidbody2D>().transform.rotation = newRotation;
        }

        counterSpread += Time.deltaTime;

        //Reset
        if(counterSpread > 4)
        {
            counterSpread = 0;
            isOnSpread = false;
        }
    }
}

