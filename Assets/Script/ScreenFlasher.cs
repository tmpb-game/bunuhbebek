﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class ScreenFlasher : MonoBehaviour
{
    public float Duration;
    [Range(0,1)]
    public float AlphaFrom;
    [Range(0, 1)]
    public float AlphaTo;
    [Range(0, 1)]
    public float AlphaStep;
    public float Interval;

    private Image target;
    private bool IsOnFlash = false;

    private void Start()
    {
        target = GetComponent<Image>();
        //StartFlashingOnce();
    }
    
    public void StartFlashingOnce()
    {
        if(target!=null)
        {
            if (!IsOnFlash)
            {
                StartCoroutine("Fade");
            }
        }
    }

    public void StartFlashingOnce(float duration, float alphafrom, float alphato, float alphastep, float interval)
    {
        if (target != null)
        {
            if (!IsOnFlash)
            {
                Duration = duration;
                AlphaFrom = alphafrom;
                AlphaTo = alphato;
                AlphaStep = alphastep;
                Interval = interval;
                
                StartCoroutine("Fade");
            }
        }
    }

    IEnumerator Fade()
    {
        int toggleColor = 0;
        IsOnFlash = true;

        for (float f = 0; f <= Duration; f += Interval)
        {
            Color c = target.color;
            if (c.a <= AlphaFrom)
                toggleColor = 1;
            else if (c.a >= AlphaTo)
                toggleColor = 0;

            if (toggleColor == 0)
                c.a -= AlphaStep;
            else if (toggleColor == 1)
                c.a += AlphaStep;

            target.color = c;
            yield return new WaitForSeconds(Interval);
        }

        Color cf = target.color;
        cf.a = 0;
        target.color = cf;
        IsOnFlash = false;
    }
}
