﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public interface IShootable
{
    void ShootObject(int damage);
}

public abstract class Duck : MonoBehaviour, IShootable
{
    public AudioSource Flap;
    public AudioSource Shooted;
    public AudioSource Falling;

    [Header("Parameter untuk tuning Duck")]
    public float Speed;
    public int HitPoints;
    public float TimeToFindCrops;
    public float FlyUpdateRate;
    
    internal int SpawnIndex;
    
    protected bool IsHitable = true;
    protected Animator duckAnimation;
    protected string shootedAnimName = "shooted";
    protected int inverterX = 1;
    protected int inverterY = -1;
    protected float boundTop;
    protected float boundBottom;
    protected float boundLeft;
    protected float boundRight;
    protected float deltaToGround = 1f;
    protected float intervalToGround = .1f;
    protected bool isDuckLookingForCrop = false;

    Transform flyAwayLocation;
    protected Transform cropLocation;

    protected virtual void Awake()
    {
        flyAwayLocation = new GameObject().transform;
        cropLocation = new GameObject().transform;
        flyAwayLocation.position = new Vector2(UnityEngine.Random.Range(2.65f, 4f), UnityEngine.Random.Range(2.9f, 4f));
        cropLocation.position = new Vector2(UnityEngine.Random.Range(-1f, 1f), 0.42f);
    }

    // Use this for initialization
    protected virtual void Start()
    {
        CameraHelper.GetCameraBound(out boundLeft, out boundRight, out boundTop, out boundBottom);

        duckAnimation = GetComponent<Animator>();

        if (Flap != null)
        { Flap.Play(); }

		TimeToFindCrops = 25;
		FlyUpdateRate = 0.1f;

        // start duck animation
        InvokeRepeating("Fly", FlyUpdateRate, FlyUpdateRate);

        // randomly change dir value
        InvokeRepeating("ChangeDirection", 1.0f, UnityEngine.Random.Range(5.0f, 8.0f));

        // end
        InvokeRepeating("DuckFindCrop", TimeToFindCrops, 0);
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if (this.duckAnimation.GetCurrentAnimatorStateInfo(0).IsName(shootedAnimName))
        {
            duckAnimation.SetBool("IsShooted", false);
        }
    }

    // Invert the dir value
    private void ChangeDirection()
    {
        inverterY = inverterY != 0 ? 0 : -1;
    }

    // Duck fly
    protected abstract void Fly();// { }

    protected abstract void FlyToTheGround();

    private void DuckFindCrop()
    {
        CancelInvoke();
        this.duckAnimation.speed = 1;
        isDuckLookingForCrop = true;
        InvokeRepeating("FlyToTheGround", intervalToGround, intervalToGround);
    }

    protected void FlyAway()
    {
        IsHitable = false;

        // boundaries control
        if (transform.position.x <= flyAwayLocation.position.x)
        {
            GetComponent<SpriteRenderer>().flipX = true;
        }
        else
        {
            GetComponent<SpriteRenderer>().flipX = false;
        }

        float step = 0.1f * deltaToGround;
        GetComponent<Rigidbody2D>().transform.position = Vector2.MoveTowards(transform.position, flyAwayLocation.position, step);

        //On target
        if ((transform.position.x == flyAwayLocation.position.x + Mathf.Epsilon || transform.position.x == flyAwayLocation.position.x - Mathf.Epsilon)
            && (transform.position.y == flyAwayLocation.position.y + Mathf.Epsilon || transform.position.y == flyAwayLocation.position.y - Mathf.Epsilon))
        {
            ReleaseTheDuck();
        }
    }

    //Shoot the duck, called from weapon
    public void ShootObject(int damage)
    {
        bool isGameOver = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().GameOver;
        if (IsHitable && !isGameOver)
        {
            NotifyScore();
            DecreaseHealth(damage);
        }
    }

    //Reduce duck health
    private void DecreaseHealth(int damage)
    {
        duckAnimation.SetBool("IsShooted", false);//clear prev

        HitPoints = HitPoints - damage;
        
        if (HitPoints <= 0)
        {
            //Dead
            KillTheDuck();
        }
        else
        {
            //SFX
            if (Shooted != null)
            { Shooted.Play(); }

            //Shoot Animation
            duckAnimation.SetBool("IsShooted", true);
        }

    }

    //Kill and destory the duck
    private void KillTheDuck()
    {
        // stop all animations
        CancelInvoke();

        //Shoot Animation
        duckAnimation.SetBool("IsDead", true);

        if (Flap != null)
        { Flap.Stop(); }

        if (Falling != null)
        { Falling.Play(); }

        //Enable Gravity
        GetComponent<Rigidbody2D>().gravityScale = 0.1f;

        NotifyDie();

        IsHitable = false;

        //Deferred Destroy
        InvokeRepeating("DestroyDuckObject", 6, 0);
    }

    private void DestroyDuckObject()
    {
        CancelInvoke();
        Destroy(gameObject);
    }

    private void ReleaseTheDuck()
    {
        CancelInvoke();

        if (Flap != null)
        { Flap.Stop(); }

        NotifyDie();

        //Deferred Destroy
        InvokeRepeating("DestroyDuckObject", 6, 0);
    }

    private void DuckEatCrop()
    {
        CancelInvoke("FlyToTheGround");

        // Leaving
        InvokeRepeating("FlyAway", 0.5f, 0.2f);
        duckAnimation.SetBool("IsLeaving", true);

        //Reduce
        NotifyPlayerHp();
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "ground" && isDuckLookingForCrop && HitPoints > 0)
        {
            DuckEatCrop();
        }
    }

    public event EventHandler ScoreHandler;

    public void NotifyScore()
    {
        EventHandler handler = ScoreHandler;
        if (handler != null)
        {
            handler(this, EventArgs.Empty);
        }
    }

    public event EventHandler DuckDieHandler;

    public void NotifyDie()
    {
        EventHandler handler = DuckDieHandler;
        if (handler != null)
        {
            handler(this, EventArgs.Empty);
        }
    }

    public event EventHandler PlayerLifePointsHandler;

    public void NotifyPlayerHp()
    {
        EventHandler handler = PlayerLifePointsHandler;
        if (handler != null)
        {
            handler(this, EventArgs.Empty);
        }
    }

}

