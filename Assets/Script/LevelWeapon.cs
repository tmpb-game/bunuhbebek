﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class LevelWeapon : MonoBehaviour
{
    public AudioSource ChangeWeaponSFX;

    //Main Attribute
    public GameObject CurrentWeapon;
    public GameObject[] AccessableWeapon;

    //Misc
    public GameObject RifleSprite;
    public GameObject RifleSelSprite;
    public GameObject CanonSprite;
    public GameObject CanonSelSprite;
	public GameObject[] AmmoStatus;

    private int selectedWeaponIndex;

    // Use this for initialization
    void Start()
    {
        for (int i = 0; i < AccessableWeapon.Length; i++)
        {
            AccessableWeapon[i] = Instantiate(AccessableWeapon[i], new Vector2(), new Quaternion());
			AccessableWeapon[i].GetComponent<Weapon>().AmmoManagerObject = AmmoStatus[i];
            AccessableWeapon[i].SetActive(false);
        }

        CurrentWeapon = AccessableWeapon[0];
        RifleSprite.SetActive(false);
        RifleSelSprite.SetActive(true);
        CanonSprite.SetActive(true);
        CanonSelSprite.SetActive(false);
        CurrentWeapon.SetActive(true);
    }

    bool init = true;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("s"))
        { SwitchWeapon(); }

        if (init)
        {
            if (AmmoStatus[0].GetComponent<AmmoManager>().Ammo == 0)
            {
                AccessableWeapon[0].SetActive(true);
                AmmoStatus[0].GetComponent<AmmoManager>().Ammo = AccessableWeapon[0].GetComponent<Rifle>().GetAmmo();
            }
            else if (AmmoStatus[1].GetComponent<AmmoManager>().Ammo == 0)
            {
                AccessableWeapon[1].SetActive(true);
                AmmoStatus[1].GetComponent<AmmoManager>().Ammo = AccessableWeapon[1].GetComponent<Canon>().GetAmmo();
            }
            else
            {
                AccessableWeapon[0].SetActive(true);
                AccessableWeapon[1].SetActive(false);
                init = false;
            }
        }
        else
        {

            if (AccessableWeapon[0].activeInHierarchy)
            {
                AmmoStatus[0].GetComponent<AmmoManager>().Ammo = AccessableWeapon[0].GetComponent<Rifle>().GetAmmo();
            }
            if (AccessableWeapon[1].activeInHierarchy)
            {
                AmmoStatus[1].GetComponent<AmmoManager>().Ammo = AccessableWeapon[1].GetComponent<Canon>().GetAmmo();
            }
        }

        //Debug.Log(AccessableWeapon[0].GetComponent<Rifle>().gameObject.name + " " 
        //    + AccessableWeapon[0].GetComponent<Rifle>().gameObject.GetComponent<Rifle>().GetAmmo());
       // Debug.Log(AccessableWeapon[1].GetComponent<Canon>().gameObject.name + " " + AccessableWeapon[1].GetComponent<Canon>().GetAmmo());
    }

    private void SwitchWeapon()
    {
        CurrentWeapon.SetActive(false);

        selectedWeaponIndex++;
        if (selectedWeaponIndex < AccessableWeapon.Length)
        { CurrentWeapon = AccessableWeapon[selectedWeaponIndex]; }
        else
        { selectedWeaponIndex = 0; CurrentWeapon = AccessableWeapon[selectedWeaponIndex]; }

        CurrentWeapon.SetActive(true);

        //SFX
        if (ChangeWeaponSFX != null)
        { ChangeWeaponSFX.Play(); }

        //Cursor
        GetComponent<AimCursorChange>().SwitchCursor(CurrentWeapon.name);

        //Button
        if ((CurrentWeapon.name.ToLower()).Contains("rifle"))
        {
            RifleSprite.SetActive(false);
            RifleSelSprite.SetActive(true);
            CanonSprite.SetActive(true);
            CanonSelSprite.SetActive(false);
        }
        else if ((CurrentWeapon.name.ToLower()).Contains("canon"))
        {
            RifleSprite.SetActive(true);
            RifleSelSprite.SetActive(false);
            CanonSprite.SetActive(false);
            CanonSelSprite.SetActive(true);
        }
    }
}
