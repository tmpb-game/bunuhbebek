﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AladinDuck : BossDuck
{
    protected int inverterX = 1;
    protected int inverterY = -1;

    //Adjust This
    private float defaultSpeed = 0.05f;
    private float maxSpeed = 0.2f;
    private int maxHp = 300;

    private TrailControl shadowTrail;
    private SpriteRenderer aladinSpriteRender;
    private bool furiousMode = false;

    // Use this for initialization
    protected override void Start()
    {
        Speed = defaultSpeed;
        HitPoints = maxHp;
        shootedAnimName = "shootedaladin";
        shadowTrail = gameObject.GetComponent<TrailControl>();
        aladinSpriteRender = gameObject.GetComponent<SpriteRenderer>();

        base.Start();

        // randomly change dir value
        InvokeRepeating("ChangeDirection", 1.0f, UnityEngine.Random.Range(5.0f, 8.0f));
    }

    float shootedDelay = 0;
    protected override void Update()
    {
        base.Update();

        if (HitPoints < (float)(maxHp / 3.0f))
        { EnterFuriousMode(); }

        if (this.duckAnimation.GetCurrentAnimatorStateInfo(0).IsName(shootedAnimName))
        {
            shootedDelay += Time.deltaTime;

            if (shootedDelay > 0.7f)
            {
                duckAnimation.SetBool("IsShooted", false);
                shootedDelay = 0;
            }
        }
    }

    private void FixedUpdate()
    {
        AladinTrail();
    }

    // Invert the dir value
    private void ChangeDirection()
    {
        inverterY = inverterY != 0 ? 0 : -1;
    }

    // Duck fly
    protected override void Act()
    {
        Vector2 position = new Vector2(Speed * inverterX, (Speed / 2.0f) * inverterY);

        float ics = GetComponent<Rigidbody2D>().transform.position.x;
        float ips = GetComponent<Rigidbody2D>().transform.position.y;

        // boundaries control
        if (ics <= boundLeft)
        { // Left
            inverterX = 1;
            GetComponent<SpriteRenderer>().flipX = true;
        }
        else if (ics >= boundRight)
        { //Right
            inverterX = -1;
            GetComponent<SpriteRenderer>().flipX = false;
        }
        if (ips <= boundBottom + 0.8f)
        {//bottom
            inverterY = 1;
        }
        else if (ips >= boundTop)
        {//top
            inverterY = -1;
        }

        // translate, not position!! To add position to previous position
        GetComponent<Rigidbody2D>().transform.Translate(position);
    }

    //Aladin Trailing Ghost
    bool speedMode = false;
    float speedDuration; int deferedRandom = 0;
    private void AladinTrail()
    {
        if (!speedMode)
        {
            Speed = defaultSpeed;

            if (deferedRandom++ == 20)
            {
                if (RandomizeTrail())
                {
                    speedMode = true;
                    shadowTrail.ActivateTrailing(furiousMode);
                    Speed = maxSpeed;
                }

                deferedRandom = 0;
            }
        }
        else
        {
            Speed = maxSpeed;
            speedDuration += Time.deltaTime;

            int dur = 0;
            if (!furiousMode)
            { dur = 2; }
            else
            { dur = 6; }

            if (speedDuration > dur)
            {
                speedMode = false; speedDuration = 0;
                shadowTrail.DeactivateTrailing();
                Speed = defaultSpeed;
            }
        }
    }

    private bool RandomizeTrail()
    {
        int[] rand = new int[5];

        //Calm Mode
        int maxRange = 1000;

        //Furious Mode
        if (furiousMode)
            maxRange = 100;

        rand[0] = UnityEngine.Random.Range(UnityEngine.Random.Range(0, 20), maxRange);
        rand[1] = UnityEngine.Random.Range(UnityEngine.Random.Range(0, 20), maxRange);
        rand[2] = UnityEngine.Random.Range(UnityEngine.Random.Range(0, 20), maxRange);
        rand[3] = UnityEngine.Random.Range(UnityEngine.Random.Range(0, 20), maxRange);
        rand[4] = UnityEngine.Random.Range(UnityEngine.Random.Range(0, 20), maxRange);

        int res = UnityEngine.Random.Range(0, 4);
        //Debug.Log(rand[0].ToString() + " " + rand[1].ToString() + " " + rand[2].ToString() + " " + rand[3].ToString() + " " + rand[4].ToString());

        if (!furiousMode)
        {
            if (rand[res] >= 500 && rand[res] <= 550)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            if (rand[res] >= 45 && rand[res] <= 55)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    //Aladin Rage Mode
    private void EnterFuriousMode()
    {
        //First Enter
        if (!furiousMode)
        { aladinSpriteRender.color = new Vector4(240, 0, 0, 1); }

        //Set Rage Mode!!
        furiousMode = true;
    }
}

