﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class CameraHelper
{
    public static void GetCameraBound(out float left, out float right, out float top, out float bottom)
    {
        Camera _maincamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        float vertExtent = _maincamera.orthographicSize;
        float horzExtent = vertExtent * Screen.width / Screen.height;

        float[] ret = new float[4];
        ret[0] = _maincamera.transform.position.x - horzExtent;//Left
        ret[1] = _maincamera.transform.position.x + horzExtent;//Right
        ret[2] = _maincamera.transform.position.y - vertExtent;//Bottom
        ret[3] = _maincamera.transform.position.y + vertExtent;//Top

        left = ret[0];
        right = ret[1];
        top = ret[3];
        bottom = ret[2];
    }

    public static Vector2 MousePosition(Camera camera)
    {
        return new Vector2(camera.ScreenToWorldPoint(Input.mousePosition).x,
                                    camera.ScreenToWorldPoint(Input.mousePosition).y);
    }
}
