﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class HealthPointSpriteManager : MonoBehaviour
{
    public GameObject[] DistractionHp;
    public GameObject[] CropsHp;

    public Sprite DistractionLife;
    public Sprite DistractionDead;

    public Sprite CropLife;
    public Sprite CropDead;

    private GameController LevelGameController;

    private void Start()
    {
        LevelGameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }

    void Update()
    {
        if(LevelGameController!=null)
        {
            DistractionHpUpdate();
            CropsHpUpdate();
        }
    }

    private void DistractionHpUpdate()
    {
        for (int i = 0; i < DistractionHp.Length; i++)
        {
            if (LevelGameController.DistractionHealthPoint > i)
            {
                DistractionHp[i].GetComponent<SpriteRenderer>().sprite = DistractionLife;
            }
            else
            {
                DistractionHp[i].GetComponent<SpriteRenderer>().sprite = DistractionDead;
            }
        }
    }

    private void CropsHpUpdate()
    {
        for (int i = 0; i < CropsHp.Length; i++)
        {
            if (LevelGameController.HealthPoint > i)
            {
                CropsHp[i].GetComponent<SpriteRenderer>().sprite = CropLife;
            }
            else
            {
                CropsHp[i].GetComponent<SpriteRenderer>().sprite = CropDead;
            }
        }
    }
}
