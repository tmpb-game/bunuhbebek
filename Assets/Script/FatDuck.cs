﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FatDuck : Duck
{
    protected override void Awake()
    {
        base.Awake();
    }

    // Use this for initialization
    protected override void Start()
    {
        base.Start();

        //Override
        Speed = 0.02f;
        HitPoints = 40;
        TimeToFindCrops = 30;
        shootedAnimName = "shootedfat";
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }

    // duck animation
    protected override void Fly()
    {
        Vector2 position = new Vector2(Speed * inverterX, Speed * inverterY);

        float ics = GetComponent<Rigidbody2D>().transform.position.x;
        float ips = GetComponent<Rigidbody2D>().transform.position.y;

        // boundaries control
        if (ics <= boundLeft)
        { // Left
            inverterX = 1;
            GetComponent<SpriteRenderer>().flipX = true;
        }
        else if (ics >= boundRight)
        { //Right
            inverterX = -1;
            GetComponent<SpriteRenderer>().flipX = false;
        }
        if (ips <= boundBottom + 0.8f)
        {//bottom
            inverterY = 1;
        }
        else if (ips >= boundTop)
        {//top
            inverterY = -1;
        }

        // translate, not position!! To add position to previous position
        GetComponent<Rigidbody2D>().transform.Translate(position);
    }

    protected override void FlyToTheGround()
    {
        // boundaries control
        if (transform.position.x <= cropLocation.position.x)
        {
            GetComponent<SpriteRenderer>().flipX = true;
        }
        else
        {
            GetComponent<SpriteRenderer>().flipX = false;
        }

        float step = 0.1f * deltaToGround;
        GetComponent<Rigidbody2D>().transform.position = Vector2.MoveTowards(transform.position, cropLocation.position, step);
    }
}

