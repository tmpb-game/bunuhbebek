﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    public Sprite[] SpriteTime;
    private float TimeLeft;

    private GameObject[] timeBoard;
    private const int MaximumTime = 999;
    private bool IsTimerStarted = false;

    void Start()
    {
        timeBoard = new GameObject[MaximumTime.ToString().Length];
        for (int i = 0; i < MaximumTime.ToString().Length; i++)
        {
            timeBoard[i] = GameObject.Find("Timer" + (i + 1).ToString());
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (TimeLeft > MaximumTime)
        { TimeLeft = MaximumTime; }
        else if (TimeLeft <= 0)
        { TimeLeft = 0; }

        char[] _timeC = ((int)TimeLeft).ToString().ToCharArray();
        Array.Reverse(_timeC);
        for (int i = 0; i < MaximumTime.ToString().Length; i++)
        {
            if (i < _timeC.Length)
            {
                timeBoard[i].GetComponent<SpriteRenderer>().sprite = SpriteTime[int.Parse(_timeC[i].ToString())];
            }
            else
            { timeBoard[i].GetComponent<SpriteRenderer>().sprite = SpriteTime[0]; }
        }

        //Check whether timer is started
        if (IsTimerStarted)
        {
            if (TimeLeft <= 0)
            { GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().LevelGameOver(); }
            else
            {
                TimeLeft -= Time.deltaTime;
            }
        }
    }

    public void SetTimer(int time)
    {
        TimeLeft = time;
    }

    public float GetTime()
    {
        return TimeLeft;
    }

    public void StartTimer()
    {
        IsTimerStarted = true;
    }

    public void StopTimer()
    {
        IsTimerStarted = false;
    }
}
