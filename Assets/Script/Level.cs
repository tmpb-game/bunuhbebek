﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    public GameObject WinPopup;

    [Header("Parameter untuk tuning Level")]
    public int LevelTime;
    public GameObject[] Enemy;
    public GameObject Boss;
    public GameObject[] Crops;
    public GameObject[] Distraction;
    public int MaximumSpawn;
    public int MaximumOnScreenSpawn;
    public float IntervalSpawn;
    public float IntervalSpawnDistraction;
    public Vector2[] RandomPosition;
    public Vector2 BossPosition;
    public Vector2 DistractionPosition;

    [HideInInspector]
    public GameObject WeaponConfig;

    private GameObject LevelText;
    private ScoreManager LevelScoreManager;
    private TimeManager LevelTimerManager;
    private GameController LevelGameController;
    private int CurrentSpawnIndex = 0;
    private int CurrentSpawnOnScreen = 0;
    private bool IsGameStarted = false;
    private int TotalEnemy;
    private int intervalBeforeGameStarted = 5;
    private bool IsBossOnScreen = false;

    private void Awake()
    {
        LevelText = GameObject.Find("LevelCaption");
        LevelText.SetActive(false);
    }

    // Use this for initialization
    void Start()
    {
        LevelScoreManager = GameObject.Find("Scoring").GetComponent<ScoreManager>();
        WeaponConfig = GameObject.Find("LevelWeapon");
        LevelTimerManager = GameObject.Find("Timer").GetComponent<TimeManager>();
        LevelTimerManager.SetTimer(LevelTime);
        LevelText.SetActive(true);

        LevelGameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        LevelGameController.HealthPoint = Crops.Length;
		TotalEnemy = MaximumSpawn;

        MusicPlayer.PlayGameMusic();
    }

    // Update is called once per frame
    float intervalStartTime = 0;
    void Update()
    {
        intervalStartTime += Time.deltaTime;
        if (intervalStartTime >= intervalBeforeGameStarted && !IsGameStarted)
        {
            LevelText.SetActive(false);
            IsGameStarted = true;
            LevelGameController.GameStarted = true;
            LevelTimerManager.StartTimer();
        }
    }
    
	bool alreadyWin=false;
    void FixedUpdate()
    {
		if (!alreadyWin) {
			SpawnController (Time.fixedDeltaTime);
		}
    }

    float timePeriodicSpawnCounter = 0;
    float timePeriodicDistractionCounter = 0;
    float bossIntroCounter = 0;
    bool isOnAlert = false;

    private void SpawnController(float deltaTime)
    {
        timePeriodicSpawnCounter += deltaTime;
        if (timePeriodicSpawnCounter >= IntervalSpawn)
        {
            Spawn();
            timePeriodicSpawnCounter = 0;
        }

        timePeriodicDistractionCounter += deltaTime;
        if (timePeriodicDistractionCounter >= IntervalSpawnDistraction)
        {
            SpawnDistraction();
            timePeriodicDistractionCounter = 0;
        }

		if (TotalEnemy <= 0
			&& LevelGameController.HealthPoint > 0 &&  LevelGameController.DistractionHealthPoint > 0
			&& !IsBossOnScreen && Boss != null) {

			bossIntroCounter += Time.fixedDeltaTime;

			if (bossIntroCounter < 2) {
				//Wait state
			} else if (bossIntroCounter < 8) {
				//Call alert

				if (!isOnAlert) {
					MusicPlayer.PlayAlertMusic ();

					ScreenFlasher sf = GameObject.Find ("AlertPanel").GetComponent<ScreenFlasher> ();
					sf.StartFlashingOnce (5.8f, 0.05f, 0.5f, 0.05f, 0.05f);
				}

				isOnAlert = true;
			} else if (bossIntroCounter < 11) {
				//Wait state
			} else {
				bossIntroCounter = 0;
				isOnAlert = false;

				MusicPlayer.PlayBossMusic ();
				SpawnBoss ();
				IsBossOnScreen = true;
			}
		} else if (TotalEnemy <= 0 && LevelGameController.HealthPoint > 0 && LevelGameController.DistractionHealthPoint > 0 && Boss == null) {
			LevelClear ();
		}
    }

    private void Spawn()
    {
        bool isGameOver = LevelGameController.GameOver;
        if (CurrentSpawnIndex < MaximumSpawn && CurrentSpawnOnScreen < MaximumOnScreenSpawn && !isGameOver && IsGameStarted)
        {
            int rand1 = UnityEngine.Random.Range(0, RandomPosition.Length);
            int rand2 = UnityEngine.Random.Range(0, RandomPosition.Length);
            int rand3 = UnityEngine.Random.Range(0, RandomPosition.Length);
            int rand4 = UnityEngine.Random.Range(0, RandomPosition.Length);
            int rand5 = UnityEngine.Random.Range(0, RandomPosition.Length);
            int[] randArr = new int[5] { rand1, rand2, rand3, rand4, rand5 };
            int rand = UnityEngine.Random.Range(0, 5);

            Enemy[CurrentSpawnIndex] = Instantiate(Enemy[CurrentSpawnIndex], RandomPosition[randArr[rand]], new Quaternion());
            Enemy[CurrentSpawnIndex].GetComponent<Duck>().SpawnIndex = CurrentSpawnIndex;
            Enemy[CurrentSpawnIndex].GetComponent<Duck>().ScoreHandler += ObserverSetScore;
            Enemy[CurrentSpawnIndex].GetComponent<Duck>().DuckDieHandler += ObserverReduceDuck;
            Enemy[CurrentSpawnIndex].GetComponent<Duck>().PlayerLifePointsHandler += ObserverSetPlayerHp;
            CurrentSpawnIndex++;
            CurrentSpawnOnScreen++;
        }
    }

    private void SpawnBoss()
    {
        Boss = Instantiate(Boss, BossPosition, new Quaternion());
        Boss.GetComponent<BossDuck>().ScoreHandler += ObserverSetBossScore;
        Boss.GetComponent<BossDuck>().DuckDieHandler += ObserverBossDie;
        Boss.GetComponent<BossDuck>().PlayerLifePointsHandler += ObserverSetPlayerHpByBoss;
    }

    int CurrentDistractionSpawnIndex;
    private void SpawnDistraction()
    {
        bool isGameOver = LevelGameController.GameOver;
        if (!isGameOver && IsGameStarted && CurrentDistractionSpawnIndex < Distraction.Length)
        {
            Distraction[CurrentDistractionSpawnIndex] = Instantiate(Distraction[CurrentDistractionSpawnIndex], DistractionPosition, new Quaternion());
            Distraction[CurrentDistractionSpawnIndex].GetComponent<Dog>().SpawnIndex = CurrentDistractionSpawnIndex;
            Distraction[CurrentDistractionSpawnIndex].GetComponent<Dog>().PlayerLifePointsHandler += ObserverSetDistractionHp;
            CurrentDistractionSpawnIndex++;
        }
    }

    private void ObserverSetPlayerHpByBoss(object sender, EventArgs e)
    {
        ReduceHp();
    }

    private void ObserverBossDie(object sender, EventArgs e)
    {
        LevelClear();

        ((BossDuck)sender).ScoreHandler -= ObserverSetBossScore;
        ((BossDuck)sender).PlayerLifePointsHandler -= ObserverSetPlayerHpByBoss;
        ((BossDuck)sender).DuckDieHandler -= ObserverBossDie;
    }

    public void ObserverSetScore(object sender, EventArgs args)
    {
        SetScore(1);
    }

    public void ObserverSetBossScore(object sender, EventArgs args)
    {
        SetScore(5);
    }

    public void ObserverSetPlayerHp(object sender, EventArgs args)
    {
        ReduceHp();
        ((Duck)sender).PlayerLifePointsHandler -= ObserverSetPlayerHp;
    }

    public void ObserverReduceDuck(object sender, EventArgs args)
    {
        TotalEnemy--;
        CurrentSpawnOnScreen--;
        ((Duck)sender).ScoreHandler -= ObserverSetScore;
        ((Duck)sender).DuckDieHandler -= ObserverReduceDuck;
    }

    public void ObserverSetDistractionHp(object sender, EventArgs args)
    {
        ReduceDistractionHp();
        ((Dog)sender).PlayerLifePointsHandler -= ObserverSetDistractionHp;
    }

    private void SetScore(int incrementscore)
    {
        LevelScoreManager.Score = LevelScoreManager.Score + incrementscore;
    }

    private void ReduceHp()
    {
        int playerHp = LevelGameController.HealthPoint;
        if (playerHp >= 0)
        {
            Destroy(Crops[playerHp]);
            playerHp--;
        }

        LevelGameController.HealthPoint = playerHp;
    }

    private void ReduceDistractionHp()
    {
        int playerHp = LevelGameController.DistractionHealthPoint;
        if (playerHp >= 0)
        {
            playerHp--;
        }

        LevelGameController.DistractionHealthPoint = playerHp;
    }

    private void LevelClear()
    {
		alreadyWin = true;

        //Win
        LevelTimerManager.StopTimer();

        //Calculate Score
        int score = LevelScoreManager.CalculateFinalScore(LevelTimerManager.GetTime(),
            LevelGameController.HealthPoint);

        //Call Victory handler here
        WinPopup.SetActive(true);
		MusicPlayer.PlayWinMusic();
        WinPopup.GetComponent<WinHandler>().AnimateScoring(score);
    }
}
