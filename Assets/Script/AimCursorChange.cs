﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimCursorChange : MonoBehaviour
{
    private Texture2D CursorTexture;

    public Texture2D RifleTexture;
    public Texture2D CanonTexture;

    // Use this for initialization
    void Start()
    {
        SwitchCursor("rifle");
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SwitchCursor(string name)
    {
        if ((name.ToLower()).Contains("rifle"))
        {
            CursorTexture = RifleTexture;
        }
        else if ((name.ToLower()).Contains("canon"))
        {
            CursorTexture = CanonTexture;
        }

        if (CursorTexture != null)
        {
            Vector2 cursorHotspot = new Vector2(CursorTexture.width / 2, CursorTexture.height / 2);
            Cursor.SetCursor(CursorTexture, cursorHotspot, CursorMode.Auto);
        }
    }
    //void OnMouseEnter()
    //{
    //    if (cursorTexture != null)
    //    {
    //        Vector2 cursorHotspot = new Vector2(cursorTexture.width / 2, cursorTexture.height / 2);
    //        Cursor.SetCursor(cursorTexture, cursorHotspot, CursorMode.Auto);
    //    }
    //}
}
