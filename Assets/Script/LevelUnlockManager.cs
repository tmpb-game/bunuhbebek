﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class LevelUnlockManager : MonoBehaviour
{

    public int LevelIdx;
    public Image Image;
    public Image[] Stars;

    void Start()
    {
        checkLevel();
    }

    void Update()
    {
        checkLevel();
    }

    private void checkLevel()
    {
        if (LevelIdx <= LevelDataManager.instance.FindLastUnlockedLevel())
        {
            LevelUnlocked();
        }
        else
        {
            LevelLocked();
        }
    }

    public void LevelSelect(string levelString)
    {
        LevelDataManager.instance.SetActiveLevel(LevelIdx);
        SceneManager.LoadScene(levelString);
    }

    void LevelLocked()
    {
        GetComponent<Button>().interactable = false;
        Image.enabled = true;
    }

    void LevelUnlocked()
    {
        GetComponent<Button>().interactable = true;
        Image.enabled = false;
        int achievedStars = LevelDataManager.instance.GetAchievedStars(LevelIdx);
        int i = 2;
        while (achievedStars - 1 < i)
        {
            Stars[i].enabled = false;
            i--;
        }
    }
}﻿
