﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ghosting : MonoBehaviour {

	private SpriteRenderer Ghost;
	private float Lifetime = 1f;

	// Use this for initialization
	void Start () {
		Ghost = GetComponent<SpriteRenderer> ();
	}

    public void ChangeColor(Vector4 color)
    { Ghost.color = color; }

	// Update is called once per frame
	void Update () {
		Lifetime -= Time.deltaTime;

		if (Lifetime <= 0)
			Destroy(gameObject);
	}
}
