﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaintainAspectRatio : MonoBehaviour
{

    private float lastWidth = 0;
    private float lastHeight = 0;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        float width = Screen.width;
        float height = Screen.height;

        if (lastWidth != width) // if the user is changing the width
        {
            // update the height
            float heightAccordingToWidth = (width / 16.0f) * 9.0f;
            Screen.SetResolution((int)width, (int)Mathf.Round(heightAccordingToWidth), false, 0);
        }
        else if (lastHeight != height) // if the user is changing the height
        {
            // update the width
            float widthAccordingToHeight = (height / 9.0f) * 16.0f;
            Screen.SetResolution((int)Mathf.Round(widthAccordingToHeight), (int)height, false, 0);
        }

        lastWidth = width;
        lastHeight = height;
    }
}
