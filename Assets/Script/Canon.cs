﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using UnityEngine;

public class Canon : Weapon
{
    // Use this for initialization
    protected override void Start()
    {
        base.Start();

        //Override
        MaximumAmmo = 6;
        Ammo = 6;
        ReloadTime = 2f;
        Damage = 2;
        Area = 0.6f;
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();

        Shoot();
    }

    public int GetAmmo()
    {
        return Ammo;
    }

    private bool shootOnProcess = false;
    protected override void Shoot()
    {
        GameController gc = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        bool isGameOver = gc.GameOver;
        bool isGameStarted = gc.GameStarted;
        if (Input.GetMouseButtonDown(0) && !isGameOver && isGameStarted && !shootOnProcess)
        {
            if (Ammo > 0)
            {
                shootOnProcess = true;

                Ammo--;

                //SFX
                if (ShootSFX != null)
                { ShootSFX.Play(); }
                if (ShootPs != null)
                {
                    ShootPs.transform.position = CameraHelper.MousePosition(Camera.main);
                    ShootPs.Simulate(1);
                    ShootPs.Play();
                }

                Camera camera = Camera.main;
                RaycastHit2D[] hit = Physics2D.CircleCastAll(CameraHelper.MousePosition(camera), Area, Vector2.zero, 0f);

                if (hit != null)
                {
                    if (CheckObstacle(hit))
                    {
                        Debug.Log("Obstacle");
                    }
                    else
                    {
                        foreach (var item in hit)
                        {
                            if (item.collider != null)
                            {
                                IShootable beib = (item.collider.gameObject).GetComponents<Component>().OfType<IShootable>().FirstOrDefault();
                                if (beib != null)
                                {
                                    beib.ShootObject(Damage);
                                    Debug.Log(item.collider.gameObject.name);
                                }
                            }
                        }
                    }
                }
                shootOnProcess = false;
            }
            else
            {
                //FX
                if (NoAmmoSFX != null)
                { NoAmmoSFX.Play(); }
            }
        }
    }

}
