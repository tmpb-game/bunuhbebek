﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using UnityEngine;

public abstract class Weapon : MonoBehaviour
{
    public AudioSource ShootSFX;
    public AudioSource RicochetSFX;
    public AudioSource NoAmmoSFX;
    public AudioSource WaitReloadSFX;
    public AudioSource ReloadSFX;
    public ParticleSystem ShootPs;
	[HideInInspector] public GameObject AmmoManagerObject;

    protected int Damage;
    protected float Area;
    protected int Ammo;
    protected int MaximumAmmo;
    protected float ReloadTime;
    protected bool IsActive;

    protected Animator shootAnimation;
    protected string shootedAnimName = "shooted";

    // Use this for initialization
    protected virtual void Start()
    {
        //shootAnimation = GetComponent<Animator>();
        
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        //if (this.shootAnimation.GetCurrentAnimatorStateInfo(0).IsName(shootedAnimName))
        //{
        //    shootAnimation.SetBool("IsShooted", false);
        //}

        if (Input.GetKeyDown("r"))
        { Reload(); }
    }
    
    protected abstract void Shoot();

    private void Reload()
    {
        //SFX
        if (WaitReloadSFX != null)
        { WaitReloadSFX.Play(); }

		if (AmmoManagerObject != null) {
			AmmoManagerObject.GetComponent<AmmoManager> ().IsOnReload = true;
		}

        InvokeRepeating("FillAmmo", ReloadTime, 0);
    }

    //Refill ammo
    private void FillAmmo()
    {
        CancelInvoke();
        Ammo = MaximumAmmo;

        //SFX
        if (ReloadSFX != null)
        { ReloadSFX.Play(); }

		if (AmmoManagerObject != null) {
			AmmoManagerObject.GetComponent<AmmoManager> ().IsOnReload = false;
		}
    }
    
    protected bool CheckObstacle(RaycastHit2D[] hit)
    {
        return hit.Any(s => s.collider.gameObject.tag.Equals("obstacle", StringComparison.InvariantCultureIgnoreCase));
    }

    //public event EventHandler ScoreHandler;

    //public void NotifyScore()
    //{
    //    EventHandler handler = ScoreHandler;
    //    if (handler != null)
    //    {
    //        handler(this, EventArgs.Empty);
    //    }
    //}
}
