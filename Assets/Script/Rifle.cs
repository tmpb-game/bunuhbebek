﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using UnityEngine;

public class Rifle : Weapon
{
    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        
        //Override
        MaximumAmmo = 12;
        Ammo = 12;
        ReloadTime = 0.5f;
        Damage = 2;
        Area = 0;

        //ShootPs = gameObject.GetComponentInChildren<ParticleSystem>();
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();

        Shoot();
    }

    public int GetAmmo()
    {
        return Ammo;
    }

    private bool shootOnProcess =  false;
    protected override void Shoot()
    {
        GameController gc = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        bool isGameOver = gc.GameOver;
        bool isGameStarted = gc.GameStarted;
        if (Input.GetMouseButtonDown(0) && !isGameOver && isGameStarted && !shootOnProcess)
        {
            if (Ammo > 0)
            {
                shootOnProcess = true;

                Ammo--;

                //FX
                if (ShootSFX != null)
                { ShootSFX.Play(); }
                if (ShootPs != null)
                { ShootPs.transform.position = CameraHelper.MousePosition(Camera.main); ShootPs.Play(); }

                RaycastHit2D[] hit = Physics2D.RaycastAll(CameraHelper.MousePosition(Camera.main), Vector2.zero, 0f);
                if (hit != null)
                {
                    if (CheckObstacle(hit))
                    {
                        if (RicochetSFX != null)
                        { RicochetSFX.Play(); }
                        Debug.Log("Obstacle");
                    }
                    else
                    {
                        foreach (var item in hit)
                        {
                            if (item.collider != null)
                            {
                                IShootable bebek = (item.collider.gameObject).GetComponents<Component>().OfType<IShootable>().FirstOrDefault();
                                if (bebek != null)
                                {
                                    bebek.ShootObject(Damage);
                                    Debug.Log(item.collider.gameObject.name);
                                }
                            }
                        }
                    }
                }

                shootOnProcess = false;
            }
            else
            {
                //FX
                if (NoAmmoSFX != null)
                { NoAmmoSFX.Play(); }
            }
        }
    }
    
}
