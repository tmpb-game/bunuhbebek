﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Dog : MonoBehaviour, IShootable
{
    public AudioSource Woof;
    public AudioSource Shooted;

    [HideInInspector]
    public int SpawnIndex;

    protected Animator dogAnimation;
    protected int inverterX = 1;
    protected float boundTop;
    protected float boundBottom;
    protected float boundLeft;
    protected float boundRight;
    protected float boundOffset = 0.5f;
    protected float deltaToGround = 0.5f;
    private float deltaToJump = 3;
    private float runLocation;

    // Use this for initialization
    protected void Start()
    {
        CameraHelper.GetCameraBound(out boundLeft, out boundRight, out boundTop, out boundBottom);
        dogAnimation = GetComponent<Animator>();
        runLocation = boundRight + 2;

        if (Woof != null)
        { Woof.Play(); }

        // start duck animation
        InvokeRepeating("Walking", 0.5f, 0.1f);
    }
    
    // Jalan
    protected void Walking()
    {
        float step = 0.1f * deltaToGround;
        GetComponent<Rigidbody2D>().transform.position = Vector2.MoveTowards(transform.position, new Vector2(runLocation, transform.position.y), step);

        //On target
        if ((transform.position.x == runLocation + Mathf.Epsilon || transform.position.x == runLocation - Mathf.Epsilon))
        {
            ReleaseTheDog();
        }
    }
    
    protected void Run()
    {
        float step = 0.5f * deltaToGround;
        GetComponent<Rigidbody2D>().transform.position = Vector2.MoveTowards(transform.position, new Vector2(runLocation, transform.position.y), step);

        //On target
        if ((transform.position.x == runLocation + Mathf.Epsilon || transform.position.x == runLocation - Mathf.Epsilon))
        {
            ReleaseTheDog();
        }
    }

    //Called from weapon
    public void ShootObject(int damage)
    {
        bool isGameOver = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().GameOver;
        if (!isGameOver)
        {
            dogAnimation.SetBool("IsShooted", true);

            //SFX
            if (Shooted != null)
            { Shooted.Play(); }

            NotifyPlayerHp();

            //START TO RUN
            CancelInvoke();
            InvokeRepeating("Run", 1f, 0.1f);
        }
    }

    private void DestroyObject()
    {
        CancelInvoke();
        Destroy(gameObject);
    }

    private void ReleaseTheDog()
    {
        CancelInvoke();

        if (Woof != null)
        { Woof.Stop(); }

        //NotifyDie();

        //Deferred Destroy
        InvokeRepeating("DestroyObject", 6, 0);
    }

    //public event EventHandler DogDieHandler;

    //public void NotifyDie()
    //{
    //    EventHandler handler = DogDieHandler;
    //    if (handler != null)
    //    {
    //        handler(this, EventArgs.Empty);
    //    }
    //}

    public event EventHandler PlayerLifePointsHandler;

    public void NotifyPlayerHp()
    {
        EventHandler handler = PlayerLifePointsHandler;
        if (handler != null)
        {
            handler(this, EventArgs.Empty);
        }
    }
}

