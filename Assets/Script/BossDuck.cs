﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public abstract class BossDuck : MonoBehaviour, IShootable
{
    public AudioSource Flap;
    public AudioSource Shooted;
    public AudioSource SmashingGround;
    public AudioSource Falling;

	public float Speed;
	public int HitPoints;
    protected bool IsHitable = true;
    
    protected Animator duckAnimation;
    protected string shootedAnimName;
    protected float boundTop;
    protected float boundBottom;
    protected float boundLeft;
    protected float boundRight;
    protected float boundOffset = 0.5f;
    protected float deltaToGround = 1f;
    
    // Use this for initialization
    protected virtual void Start()
    {
        CameraHelper.GetCameraBound(out boundLeft, out boundRight, out boundTop, out boundBottom);

        duckAnimation = GetComponent<Animator>();

        if (Flap != null)
        { Flap.Play(); }

        // start duck animation
        InvokeRepeating("Act", 0.5f, 0.1f);
    }

    protected virtual void Update()
    {
        //NOP
    }

    // Duck fly
    protected abstract void Act();
    
    //Shoot the duck, called from weapon
    public void ShootObject(int damage)
    {
        bool isGameOver = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().GameOver;
        if (IsHitable && !isGameOver)
        {
            NotifyScore();
            DecreaseHealth(damage);
        }
    }

    //Reduce duck health
    private void DecreaseHealth(int damage)
    {
        duckAnimation.SetBool("IsShooted", false);//clear prev

        HitPoints = HitPoints - damage;
        
        if (HitPoints <= 0)
        {
            //Dead
            KillTheDuck();
        }
        else
        {
            //SFX
            if (Shooted != null)
            { Shooted.Play(); }

            //Shoot Animation
            duckAnimation.SetBool("IsShooted", true);
        }

    }

    //Kill and destory the duck
    private void KillTheDuck()
    {
        // stop all animations
        CancelInvoke();

        //Shoot Animation
        duckAnimation.SetBool("IsDead", true);

        if (Flap != null)
        { Flap.Stop(); }

        if (Falling != null)
        { Falling.Play(); }

        //Enable Gravity
        GetComponent<Rigidbody2D>().gravityScale = 0.1f;

        NotifyDie();

        IsHitable = false;

        //Deferred Destroy
        InvokeRepeating("DestroyDuckObject", 6, 0);
    }

    private void DestroyDuckObject()
    {
        CancelInvoke();
        Destroy(gameObject);
    }

    private void ReleaseTheDuck()
    {
        CancelInvoke();

        if (Flap != null)
        { Flap.Stop(); }

        NotifyDie();

        //Deferred Destroy
        InvokeRepeating("DestroyDuckObject", 6, 0);
    }
    
    public event EventHandler ScoreHandler;

    public void NotifyScore()
    {
        EventHandler handler = ScoreHandler;
        if (handler != null)
        {
            handler(this, EventArgs.Empty);
        }
    }

    public event EventHandler DuckDieHandler;

    public void NotifyDie()
    {
        EventHandler handler = DuckDieHandler;
        if (handler != null)
        {
            handler(this, EventArgs.Empty);
        }
    }

    public event EventHandler PlayerLifePointsHandler;

    public void NotifyPlayerHp()
    {
        EventHandler handler = PlayerLifePointsHandler;
        if (handler != null)
        {
            handler(this, EventArgs.Empty);
        }
    }
}

